FROM python:3.7-alpine
MAINTAINER Brian Njoroge

ENV PYTHONUNBUFFERED 1


RUN mkdir /app
WORKDIR /app
COPY . /app/

# install dependencies
RUN apk update && \
    apk add --virtual build-deps gcc python-dev musl-dev && \
    apk add postgresql-dev && \
    apk add netcat-openbsd

RUN apk add --no-cache jpeg-dev zlib-dev
RUN apk add --no-cache --virtual .build-deps build-base linux-headers \
    && pip install Pillow

#RUN ln -s /usr/lib/x86_64-linux-gnu/libz.so /lib/
#RUN ln -s /usr/lib/x86_64-linux-gnu/libjpeg.so /lib/
RUN pip install -U pip

RUN pip install -r requirements.txt


RUN adduser -D user
USER user