from django.urls import path

from rest_framework.urlpatterns import  format_suffix_patterns

from . import views

urlpatterns = [
    path('',views.index,name='listings'),
    path('<int:listing_id>',views.listing,name='listing'),
    path('search',views.search,name='search'),
    path('listings',views.ListingList.as_view(),name='listingss'),
    path('listings/<int:pk>',views.ListingList.as_view())

]

urlpatterns = format_suffix_patterns(urlpatterns)