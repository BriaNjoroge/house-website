from django.contrib import admin

from .models import Realtor

class RealtorAdmin(admin.ModelAdmin):
    list_display = ('id','name','email','hire_date','is_mvp')
    list_editable = ('is_mvp',)
    list_display_links = ('id','name')
    list_per_page = 10
    # list_filter = ('name',)
    search_fields = ('name','phone','email')

admin.site.register(Realtor,RealtorAdmin)