from django.shortcuts import render,get_object_or_404

from .models import Realtor


# imports for django rest framework
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .serializers import RealtorSerializer
from django.http import Http404

class RealtorList(APIView):
    """
        Retrieve, update or delete a snippet instance.
        """

    def get_object(self, pk):
        try:
            return Realtor.objects.get(pk=pk)
        except Realtor.DoesNotExist:
            raise Http404

    def get(self,request):
        realtors = Realtor.objects.all()
        serializer = RealtorSerializer(realtors,many=True)

        return Response(serializer.data)

    # def get(self,request,pk):
    #     realtors = self.get_object(pk)
    #     serializer = RealtorSerializer(realtors)
    #
    #     return Response(serializer.data)

    def post(self, request):
        serializer = RealtorSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = RealtorSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)