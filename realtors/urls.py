from django.urls import path

from rest_framework.urlpatterns import  format_suffix_patterns

from . import views

urlpatterns = [
path('realtors',views.RealtorList.as_view(),name='realtors'),
path('realtors/<int:pk>',views.RealtorList.as_view())


]

urlpatterns = format_suffix_patterns(urlpatterns)