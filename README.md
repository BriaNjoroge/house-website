# Real Estate Management System

This system allows users to to see the houses available and communicate with realtors to book the houses

## Getting Started
This is my portfolio page
### Prerequisites

Install Postgres database in your system.<br>
<br>For Windows users can follow this [link](https://www.postgresqltutorial.com/install-postgresql/)
<br>For Linux Users users can follow this [link](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-postgresql-on-ubuntu-18-04)


### Installing

Installing the libraries needed, run the following command in your cmd:

Create a virtual environment

```
virtualenv venv
```

Install the libraries needed

```
pip install -r requirements.txt
```


## Deployment
#### Localhost

To run the project in your localhost:

```
python manage.py migrate
```
```
python manage.py runserver
```

#### Docker

To run the project as a docker container:
<br>In the settings file change<br> DOCKER = True
```
docker-compose run web 
```
grab a cup of cofee in the mean time as you wait for it to build the images
## Built With

* [Dropwizard](http://www.dropwizard.io/1.0.2/docs/) - The web framework used
* [Maven](https://maven.apache.org/) - Dependency Management
* [ROME](https://rometools.github.io/rome/) - Used to generate RSS Feeds

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.



## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Brad Travesty -  through his tutorials, helped me learn django
